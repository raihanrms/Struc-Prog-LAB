// Program to display a number if users enters negative number
// If users enters positive number, that number won't be displayed

#include <stdio.h>
int main(void)
{
    int number;

    printf("Enter an integer: ");
    scanf("%d", &number);

    // Test expression is true if number is less than 0
    if (number < 0)
        {
            printf("You have entered %d.\n", number);
        }
    printf("The If statement is easy.");

    return 0;
}
