// Program to relate two integer using =, > or <
#include <stdio.h>
int main(void)
{
    int number1, number2;

    printf("Enter first integer: ");
    scanf("%d", &number1);
    printf("Enter second integer: ");
    scanf("%d", &number2);

    // checks id two integers are equal.
    if(number1 == number2)
    {
        printf("Result: %d = %d",number1,number2);
    }

    // checks if number1 is grater than number2.
    else if (number1 > number2)
    {
        printf("Result: %d > %d", number1, number2);
    }

    // id both test expression is false
    else
    {
        printf("Result: %d < %d",number1, number2);
    }
    return 0;
}
