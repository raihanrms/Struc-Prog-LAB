// Program to check weather an integer entered by the user is odd or even

#include<stdio.h>
int main(void)
{
    int number;
    printf("Enter an integer: ");
    scanf("%d",&number);

    // True id remainder is 0
    if( number%2 == 0)
        printf("%d is an even integer.",number);
    else
        printf("%d is an odd integer.",number);
    return 0;
}
