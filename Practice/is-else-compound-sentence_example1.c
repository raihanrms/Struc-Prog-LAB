#include<stdio.h>

int main(void)
{
    int i = 100;

    printf("A single statement\n");

    {
        // A compound statement
        printf("A statement inside compound statement\n");
        printf("Another statement inside compound statement\n");
    }

    // Signal to OS that everything works fine
    return 0;
}
