// two-funtion.c is a program that uses two funtions in one file //
#include<stdio.h>
void butler(void);          // ISO/ANSI C funtion prototyping //

int main(void)
{
    printf("I will summon the butler funtion. \n");
    butler();
    printf("Yes. Bring me some tea and writable CD-ROMS.\n");

    return 0;
}

void butler(void) /* start of funtion defination */
{
    printf("You rang, sir?\n");
}
